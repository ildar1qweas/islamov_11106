﻿using System;

namespace CW3_Variant2
{
    public class RationalMatrix2x2
    {
        public RationalMatrix2x2(int[,] matrix)
        {
            N = 2;
            rationalMatrix = new int[N, N];

            for (int i = 0; i < N; i++)
                for (int j = 0; j < N; j++)
                    rationalMatrix[i, j] = matrix[i, j];
        }

        public RationalMatrix2x2()
        {
            N = 2;
            rationalMatrix = new int[N, N];
        }

        private int[,] rationalMatrix;
        public int N { get; private set; }

        public static RationalMatrix2x2 Umn(RationalMatrix2x2 a, RationalMatrix2x2 b)
        {
            RationalMatrix2x2 matrix2X2 = new RationalMatrix2x2();
            for (int i = 0; i < a.N; i++)
                for (int j = 0; j < b.N; j++)
                    for (int k = 0; k < b.N; k++)
                        matrix2X2.rationalMatrix[i, j] += a.rationalMatrix[i, k]*b.rationalMatrix[k, j];

            return matrix2X2;
        }
        
        public static RationalMatrix2x2 Addition(RationalMatrix2x2 matrix1, RationalMatrix2x2 matrix2)
        {
            RationalMatrix2x2 matrix2X2 = new RationalMatrix2x2();
            for (int i = 0; i < matrix2X2.N; i++)
                for (int j = 0; j < matrix2X2.N; j++)
                    matrix2X2.rationalMatrix[i, j] = matrix1.rationalMatrix[i, j] + matrix1.rationalMatrix[i, j];

            return matrix2X2;
        }

        public static RationalMatrix2x2 UmnNumber(RationalMatrix2x2 matrix2X2, int number)
        {
            for (int i = 0; i < matrix2X2.N; i++)
                for (int j = 0; j < matrix2X2.N; j++)
                    matrix2X2.rationalMatrix[i, j] *= number;
            return matrix2X2;
        }

        public static int FindDeterminant(RationalMatrix2x2 matrix2X2)
        {
            return matrix2X2.rationalMatrix[0, 0] * matrix2X2.rationalMatrix[1, 1] -
                   matrix2X2.rationalMatrix[0, 1] * matrix2X2.rationalMatrix[1, 0];
        }
    }
}