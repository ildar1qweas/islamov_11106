﻿using System;
using System.Data;

namespace CW3_Variant2
{
    class Program
    {
        static void Main(string[] args)
        {
            RationalMatrix2x2 firstMatrix2X2 = new RationalMatrix2x2(new int[2, 2] {{1, 2}, {3, 4}});
            RationalMatrix2x2 secondMatrix2X2 = new RationalMatrix2x2(new int[2, 2] {{1, 2}, {3, 4}});
            
            RationalMatrix2x2.Umn(firstMatrix2X2, secondMatrix2X2);
            RationalMatrix2x2.Addition(firstMatrix2X2, secondMatrix2X2);

            RationalMatrix2x2.FindDeterminant(firstMatrix2X2);
        }
    }
}
