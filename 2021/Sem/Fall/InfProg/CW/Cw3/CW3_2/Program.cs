﻿using System;
using System.Linq.Expressions;

namespace CW3_2
{
    interface IHasInfo
    {
        string FirstName { get; set; }

        string LastName { get; }
        
        void ShowInfo();
    }

    interface IWork 
    {
        string Job { get; }
        int Salary { get; }
    }
    
    abstract class Person :  IWork, IHasInfo
    {

        public virtual string Job { get; set;}

        public virtual int Salary { get; set;}
        public abstract string FirstName { get; set; }
        
        public abstract string LastName { get; set;}

        public void ShowInfo()
        {
            Console.WriteLine($"Имя : {FirstName}\t Фамилия: {LastName}");
        }
        
    }

    class Employee : Person 
    {
        public override string Job { get; set;}
        public override string FirstName { get; set; }
        public override string LastName { get; set; }
    }

    class Teacher : Employee
    {
        public Teacher()
        {
            Job = GetType().Name;
        }
        public override int Salary => 50000;

        public void Teach()
        {
            Console.WriteLine("Я учу!");
        }
    }

    class Security : Employee
    {
        public override int Salary => 40000;

        public void Guard()
        {
            Console.WriteLine("Я охраняю!");
        }
    }

    class Student : Person
    {

        public override string FirstName { get; set;}
        public override string LastName { get; set;}

        public void Study()
        {
            Console.WriteLine("Я учусь!");
        }
    }

    class Persons
    {
        public void GetAllInformation(Person person)
        {
            person.ShowInfo();
            Console.Write($"\t Должность: {person.Job}\tЗарплата: {person.Salary}");
        }
        
        public void PrintFullName(Person person)
        {
            person.ShowInfo();
        }
    }
    
    class Program
    {
        static void Main(string[] args)
        {
            Persons1 person1 = new Persons1();
            Student student = new Student() {FirstName = "Владимир", LastName = "Владимиров"};
            Teacher teacher = new Teacher() {FirstName = "Ольга", LastName = "Ильина"};
            Security security = new Security() { };
            Person[] persons = {student, teacher, security};

            foreach (var person in persons)
            {
                //persons1.
                //persons1.
            }
        }
    }
}