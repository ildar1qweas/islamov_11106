﻿namespace CW3
{
    public class Rectangle
    {
        public Rectangle(double x, double y)
        {
            X = x;
            Y = y;
        }

        private double X { get;}
        private double Y { get; }

    }
}