using System;

namespace CW2
{
    class Program
    {
        //Variant 1
        static void Main(string[] args)
        {
            //var arrayOfWords = Console.ReadLine().Split(" ");
            //WriteArrayElements(Task1(arrayOfWords));
            Task2(10, 2);
        }

        static void Task2(int k, int n)
        {
            if (n < 9)
            {
                {
                    if (n == 9) Console.WriteLine(n * k);
                }

                Console.WriteLine();
                Task2(k, n + 1);
            }
        }

        static string[] Task1(string[] arrayOfWords)
        {
            /*Тесты:
             1. Проверка на null - NullException
             2. Проверка на пустую строку
             3. Проверка на верность вывода - если количество введённых слов чётно, то последнее будет слово будет "false"
             если нечётно, то - "true"
             4. 
             */

            int countOfEntry = 1;
            for (int i = 0; i < arrayOfWords.Length; i++)
            {
                if (arrayOfWords[i] == "true")
                {
                    if (countOfEntry % 2 == 0)
                        arrayOfWords[i] = "false";
                    else
                        arrayOfWords[i] = "true";
                }
                countOfEntry++;
            }
            return arrayOfWords;
        }

        static void WriteArrayElements(string[] array)
        {
            foreach (var words in array)
            {
                Console.WriteLine(words);
            }
        }
    }
}