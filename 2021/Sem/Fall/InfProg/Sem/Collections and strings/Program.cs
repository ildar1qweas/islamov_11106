﻿using System;


namespace Collections_strings
{
    class Program
    {
        static void Main()
        {
            //Col1();
            //Col2();
            //Col3();
            //Col4();
        }

        static void Col1()
        {
            var k = int.Parse(Console.ReadLine());
            var l = int.Parse(Console.ReadLine());
            var r = int.Parse(Console.ReadLine());
            Random random = new Random();
            int[] array = new int[k];
            var sumL = 0;
            var sumR = 0;
            for (var i = 0; i < k; i++)
            {
                array[i] = random.Next(0, 100);
            }
            for (var i = 0; i < l; i++)
            {
                sumL += array[i];
            }
            for (var i = 0; i < r; i++)
            {
                sumR += array[i];
            }
            Console.WriteLine(sumR-sumL);
            foreach (var e in array)
            {
                Console.WriteLine(e);
            }
        }

        static void Col2()
        {
            int[] array = new int[50];
            var k = int.Parse(Console.ReadLine());
            var l = int.Parse(Console.ReadLine());
            var r = int.Parse(Console.ReadLine());
            var x =int.Parse(Console.ReadLine());
            for (int i = 0; i < k; i ++)
            {
                array[l] = x;
                if (r + 1 < array.Length) array[r + 1] = -x;
            }

            for (int i = 1; i < array.Length; i++)
            {
                array[i] = array[i - 1] + array[i];
               // Console.WriteLine(array[i]);
            }
            Console.WriteLine(array[l]);
            Console.WriteLine(array[r]);
        }

        static void Col3()
        {
            Random rnd = new Random();
            var m = int.Parse(Console.ReadLine());
            int[] array = new int[6]{-5, 5, -10, 8,- 12, 20};
            /*for (var i = 0; i < array.Length; i++)
            {
                array[i] = rnd.Next(-100, 100);
            }*/

            int sum = 0;
            int maxSum = 0;
            for (var i = 0; i < array.Length; i++)
            {
                sum += array[i];
                if (sum < 0)
                {
                    sum = 0;
                }
                if (maxSum < sum)
                    maxSum = sum;
            }

            Console.WriteLine(maxSum);
        }

        static void Col4()
        {
            var m = 4;
            int[] array = new int[9]{5, 4, 3, -10, -11, 12, -14, 12, 5};
            int sum = 0;
            int maxSum = 0;
            for (int k = 0;k < m; k++)
            {
                sum += array[k];
            }

            Console.WriteLine(sum);
            
            for (var i = m; i < array.Length; i++)
            {
                sum = sum + array[i] - array[i-m];
                if (maxSum < sum)
                    maxSum = sum;
            }

            Console.WriteLine(maxSum);
        }
    }
}