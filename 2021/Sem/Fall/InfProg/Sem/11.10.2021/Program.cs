using System;

namespace Ulearn1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Практика,Ulearn (Задачи на семинар: Loops3)
            double index = double.Parse(Console.ReadLine());
            //int x = ((int)Math.Log10((double)number) + 1);
            double number = 0;
            double pow = 1; 
            double count = 1;
            while (number + 9 * count * pow < index)
            {
                number += 9 * count * pow;
                count *= 10;
                pow++;
            }
            Console.WriteLine(number);
            Console.WriteLine(pow);
            double c = Math.Ceiling(((index - number) / pow));
            Console.WriteLine(c);
            double necessaryNumber = index - (pow * (c-1));
            Console.WriteLine(necessaryNumber);
            double required = Math.Pow(10, pow - 1) + c - 1;
            double i = 0;
            while (i < pow - necessaryNumber)
            {
                required /= 10;
                i++;
            }
            required %= 10;
            int req = (int)required;
            Console.WriteLine(req);
        }
    }
}