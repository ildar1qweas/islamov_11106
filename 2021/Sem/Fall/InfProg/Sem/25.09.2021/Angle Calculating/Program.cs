﻿using System;
using System.Data;

namespace Angle_Hour_Min
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(FindingAngle(6, 15));
        }

        static double FindingAngle(double hour, double min)
        {
            double angle1 = Math.Abs((hour * 30 + min * 0.5) - min * 6) % 360;
            double angle2 = Math.Abs(360 - angle1);
            double angleMin = Math.Min(angle1, angle2);
            return angleMin;
        }
    }
}