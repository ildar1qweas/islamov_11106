﻿using System;

namespace Comfort_of_passengers
{
    class Program
    {
        static double inaccuracy = 0.000001;
        static void Main(string[] args)
        {
            string[] str = Console.ReadLine().Split(" ");
            double h = Convert.ToDouble(str[0]);
            double t = Convert.ToDouble(str[1]);
            double v = Convert.ToDouble(str[2]);
            double x = Convert.ToDouble(str[3]);
            double xTime = (h / x);
            double minTime;
            double maxTime;
            if (xTime <= t)
            {
                minTime = 0;
                maxTime = h / (x + inaccuracy);
                Console.WriteLine(minTime);
                Console.WriteLine(maxTime);
                return;
            }

            xTime = Math.Abs((v * t - h) / (v - x));
            maxTime = t;
            minTime = maxTime - xTime;
            Console.WriteLine(minTime);
            Console.WriteLine(maxTime);
        }
    }
}