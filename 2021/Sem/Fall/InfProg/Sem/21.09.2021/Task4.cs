using System;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int x = int.Parse(Console.ReadLine());
            int y = int.Parse(Console.ReadLine());
            int xNumber = (n-1) / x;
            int yNumber = (n-1) / y;
            int xyNumber = (n - 1) / (x * y);
            Console.WriteLine(xNumber + yNumber - xyNumber);

        }
    }
}