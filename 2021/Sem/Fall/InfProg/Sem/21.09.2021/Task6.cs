using System;

namespace Task6
{
    class Program
    {
        static double DistanceCalculate(double ax, double ay, double bx, double by, double x, double y)
            // Расстояние от точки (x,y) до отрезка AB с координатами A(ax,ay), B(bx,by)
        {
            double a = DistanceCalculate(x, y, ax, ay);
            double b = DistanceCalculate(x, y, bx, by);
            double c = DistanceCalculate(ax, ay, bx, by);
            if (c == 0)
                return 0;
            if (a >= b + c) return b;
            if (b >= a + c) return a;
 
            double p = (a + b + c) / 2.0;
            double s = Math.Round(Math.Sqrt((p - a) * (p - b) * (p - c) * p));
 
            return s * 2.0 / c;
        }

        static double DistanceCalculate(double ax, double ay, double bx, double by)
        {
            return Math.Sqrt((bx - ax)*(bx - ax) + (by - ay)*(by - ay));
        }

        static void Main(string[] args)
        {
            Console.WriteLine(DistanceCalculate(1,-5, 1, 1, 0, 0));
        }
    }
}