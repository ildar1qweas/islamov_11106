using System;
using System.Xml;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var c = LeapYearsBetween(int.Parse(Console.ReadLine()), int.Parse(Console.ReadLine()));
            Console.WriteLine(c);
        }

        static int LeapYearsBetween(int year1, int year2)
        {
            var y1 = new DateTime(year1, 1, 1);
            var y2 = new DateTime(year2, 1, 1);
            var nonLeapDays = 365 * (y2.Year - y1.Year);
            var leapDays = (y2 - y1).Days - nonLeapDays;
            return leapDays;
        }
    }
}