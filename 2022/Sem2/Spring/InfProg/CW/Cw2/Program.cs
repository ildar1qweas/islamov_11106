﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Cw_2_sem_2
{
    class Program
    {
        record Photo(int AlbumId, int Id, string title, string url, string thumbnailUrl);
        public static async Task TakeImages()
        {
            using var client = new HttpClient();
            var url = "https://jsonplaceholder.typicode.com/photos";
            var res = await client.GetStringAsync(url);
            using JsonDocument doc = JsonDocument.Parse(res);
            JsonElement root = doc.RootElement;
            var photos = root.EnumerateArray();
            List<Photo> list = new List<Photo>();
            
            foreach (var photo in photos)
                list.Add(JsonSerializer.Deserialize<Photo>(photo.ToString() ?? string.Empty));
            var adresses = list.Select(photo => photo.thumbnailUrl).ToList();
            
            client.DefaultRequestHeaders.UserAgent.Add(new ProductInfoHeaderValue(new ProductHeaderValue("C#-program")));
            
            for (int i = 0; i < adresses.Count; i++)
            {
                Task.Delay(100);
                var bytes = await client.GetByteArrayAsync(adresses[i]);
                await File.AppendAllTextAsync("photo.txt", bytes.ToString());
            }
        }
        
        
        static async Task Main(string[] args)
        {
            await TakeImages();
        }
    }
}