﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ForCW
{

    class Program
    {
        record Person(string Name, string Company);
        record Company(string Title, string Language);
        
        public static int GetDiscount(IEnumerable<Discount> discounts, int consumerCode, string shopName)
        {
            var discount = discounts.FirstOrDefault(discount => discount.ConsumerCode == consumerCode && discount.ShopName == shopName);

            return discount != default ? discount.SizeOfDiscount : 0;
        }
        
        static void Main(string[] args)
        {
            

            List<Product> products = new List<Product>() //B
            {
                new Product("AB223-2323", "Household goods", "Nilfgaard"),
                new Product("BC426-5332", "Household goods", "Redania"),
                new Product("AD411-1034", "Alcohol", "Skellige")
            };

            

            List<ProductPrice> prices = new List<ProductPrice>() //D
            {
                new ProductPrice("AB223-2323", 250, "5rka"),
                new ProductPrice("BC426-5332", 1000, "5rka"),
                new ProductPrice("AD411-1034", 500, "Magnit"),
                new ProductPrice("AB223-2323", 233, "Magnit"),
            };

            List<InformationAboutPurchase> purchases = new List<InformationAboutPurchase>() //E
            {
                new InformationAboutPurchase("AB223-2323", 1234, "5rka"),
                new InformationAboutPurchase("BC426-5332", 1234, "5rka"),
                new InformationAboutPurchase("BC426-5332", 1234, "5rka"),
                new InformationAboutPurchase("AD411-1034", 11, "Magnit"),
                new InformationAboutPurchase("AB223-2323", 5050, "5rka"),
                new InformationAboutPurchase("BC426-5332", 5050, "5rka")
            };

            

            var purchaseOfEveryone = products.Join(purchases,
                pr => pr.Article,
                p => p.Article,
                (pr, p) => new { 
                    CountOfCategories = products.Count(product => product.Article == p.Article),
                    ConsumerCode = p.ConsumerCode,
                    MaxPrice = prices.Where(price => price.Article == pr.Article)
                        .Select(price => price.Price).Max()
                })
                .OrderByDescending(information => information.CountOfCategories)
                .ThenBy(information => information.ConsumerCode);

            foreach (var purchase in purchaseOfEveryone)
                Console.WriteLine(purchase);

            
        }
    }
}