﻿using System.Collections.Generic;

namespace ForCW
{
    public class ShopAggregate
    {
        public List<Consumer> Consumers;
        public List<Product> Products;
        public List<Discount> Discounts;
        public List<ProductPrice> Prices;
        public List<InformationAboutPurchase> Information;

        public ShopAggregate(List<Consumer> consumers, List<Product> products,
            List<Discount> discounts, List<ProductPrice> prices, List<InformationAboutPurchase> information)
        {
            Consumers = consumers;
            Products = products;
            Prices = prices;
            Discounts = discounts;
            Information = information;
        }
    }
}