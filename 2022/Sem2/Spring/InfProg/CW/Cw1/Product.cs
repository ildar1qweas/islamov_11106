﻿using System;
using System.Text.RegularExpressions;
using System.Xml.Schema;

namespace ForCW
{
    public class Product
    {
        public string Article { get; set; }
        public string Category { get; set; }
        public string CountryOfProduction { get; set; }
        

        public Product(string article, string category, string countryOfProduction)
        {
            Category = category;
            CountryOfProduction = countryOfProduction;
            Article = article;
            
        }
    }
}