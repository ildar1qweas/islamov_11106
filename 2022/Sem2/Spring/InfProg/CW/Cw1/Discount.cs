﻿namespace ForCW
{
    public class Discount
    {
        public int ConsumerCode { get; private set; }
        public string ShopName { get; private set; }
        public int SizeOfDiscount { get; private set; }

        public Discount(int consumerCode, string shopName, int sizeOfDiscount)
        {
            ConsumerCode = consumerCode;
            ShopName = shopName;
            SizeOfDiscount = sizeOfDiscount;
        }
    }
}