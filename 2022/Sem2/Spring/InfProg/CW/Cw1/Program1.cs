﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CW12._04._2022
{
    public static class Extensions
    {
        public static Tuple<IEnumerable<Tuple<T, T>>,IEnumerable<Tuple<T, T>>> Partition<T>(this IEnumerable<T> sequence1,
            IEnumerable<T> sequence2, Func<T, bool> predicate)
        {
            List<Tuple<T,T>> enumerable1 = new List<Tuple<T,T>>();
            List<Tuple<T,T>> enumerable2 = new List<Tuple<T,T>>();
            foreach (var item1 in sequence1)
            {
                foreach (var item2 in sequence2)
                {
                    if (predicate(item1) && predicate(item2)) enumerable1.Add(Tuple.Create(item1, item2));
                    else if (!predicate(item1) && !predicate(item2)) enumerable2.Add(Tuple.Create(item1,item2));
                }
            }
            return Tuple.Create((IEnumerable<Tuple<T,T>>)enumerable1, (IEnumerable<Tuple<T,T>>)enumerable2);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            List<int> listA = new List<int>() {10, 11, 323, 2435, 446452446, 23, 54533, 3232};
            List<int> listB = new List<int>() {10001, 10002, 303, 343, 33333, 10000, 100010001};
            var pairs = listA.Select(item => )
        }
        
        
    }
}