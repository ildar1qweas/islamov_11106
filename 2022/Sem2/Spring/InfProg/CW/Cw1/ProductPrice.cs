﻿using System;
using System.Text.RegularExpressions;

namespace ForCW
{
    public class ProductPrice
    {
        public string Article { get; private set; }
        public string ShopName { get; private set; }
        public int Price { get; private set; }

        
        public ProductPrice(string article, int price, string shopName)
        {
            ShopName = shopName;
            Price = price;
            Article = article;
            
        }
    }
}