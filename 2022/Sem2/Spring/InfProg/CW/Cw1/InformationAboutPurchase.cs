﻿using System;
using System.Text.RegularExpressions;

namespace ForCW
{
    public class InformationAboutPurchase
    {
        public int ConsumerCode { get; private set; }
        public string Article { get; private set; }
        public string ShopName { get; private set; }

        
        public InformationAboutPurchase(string article, int consumerCode, string shopName)
        {
            ShopName = shopName;
            ConsumerCode = consumerCode;
            Article = article;
           
        }
    }
}