﻿namespace ForCW
{
    public class Consumer
    {
        public int ConsumerCode { get; private set; }
        public int BirthYear { get; private set; }
        public string Residence { get; private set; }

        public Consumer(int consumerCode, int birthYear, string residence)
        {
            ConsumerCode = consumerCode;
            BirthYear = birthYear;
            Residence = residence;
        }
    }
}