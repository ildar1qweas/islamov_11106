﻿using System;
using System.Collections.Generic;

namespace VersionStack
{
    
    public class VersionStack<T>
    {
        private List<StackItem<T>> ListOfHeads;
        public class StackItem<T>
        {
            public T Value { get; set; }
            public StackItem<T> Next { get; set; }

            public StackItem(T value)
            {
                Value = value;
            }
        }

        public VersionStack()
        {
            ListOfHeads = new List<StackItem<T>>();
        }

        private StackItem<T> Head { get; set; }
        private bool IsEmpty
        {
            get => (Head == null);
        }

        public void Push(T value)
        {
            StackItem<T> node = new StackItem<T>(value);
            node.Next = Head;
            Head = node;
            ListOfHeads.Add(node);
        }

        public T Pop()
        {
            if (IsEmpty) throw new InvalidOperationException();
            StackItem<T> temple = Head;
            Head = temple.Next;
            if(Head != null)
                ListOfHeads.Add(Head);
            return temple.Value;
        }

        public void Forget()
        {
            ListOfHeads = null;
        }

        public void RollBack(int n)
        {
            if(n < 0 || n > ListOfHeads.Count) throw new ArgumentException();
            Head = ListOfHeads[n];
            ListOfHeads.Add(Head);
        }
    }
}