﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace BaconCipher
{
    class Program
    {
        static void Main(string[] args)
        {
            var tests = new List<Func<string, int>>();
            var stringArray = new[]
            {
                "rthfdffdfdfffffdddffdgdfdfdg",
                "hshhflgseiuajsliouew",
                "gggfffgggghhhhghh",
                "aaaabbbbfghjjlltrgttcdssdbnpoiuyewtdarsuorprpooeuyywdgfxsdkkvcjbidfryieiue"
            };
            int count = 0;

            tests.Add(CipherTest(FindBaconCipher,349));
            tests.Add(CipherTest(FindBaconCipher,202));
            tests.Add(CipherTest(FindBaconCipher,126));
            tests.Add(CipherTest(FindBaconCipher,2713));

            foreach (var func in tests)
            {
                Console.WriteLine(func(stringArray[count]));
                count++;
            }
        }

        public static Func<string, int> CipherTest(Func<string, int> func, int count)
        {
            return str =>
            {
                Debug.Assert(count == func(str));
                return func(str);
            };
        }

        public static int FindBaconCipher(string str)
        {
            var setOfStrings = new HashSet<string>();
            for (int i = 0; i < str.Length; i++)
                for (int j = i; j < str.Length; j++)
                    setOfStrings.Add(str.Substring(i, j-i+1));   
            
            return setOfStrings.Count;
        }
    }
}