﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Loader;

namespace LRU
{
    public class LRU<T>
    {
        private Dictionary<int, T> dictionary;

        private LinkedList<int> listOfKeys;

        public LRU(int counter)
        {
            dictionary = new Dictionary<int, T>();
            listOfKeys = new LinkedList<int>();
        }

        public void Add(int key, T value)
        {
            dictionary.Add(key, value);
            listOfKeys.AddLast(key);
        }

        public T Get(int key)
        {
            if (!dictionary.ContainsKey(key)) throw new Exception("Dictionary doesn't contain this key!");
            listOfKeys.Remove(key);
            listOfKeys.AddLast(key);
            return dictionary[key];
        }

        public void RemoveLeastRecentlyUsed()
        {
            if (dictionary.Count == 0) throw new InvalidOperationException();
            var element = listOfKeys.First.Value;
            listOfKeys.RemoveFirst();
            dictionary.Remove(element);
        }
    }
}