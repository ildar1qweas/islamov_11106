using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GetMaxElementOfStack;

namespace TestStackWithMax
{
    [TestClass]
    public class UnitTest1
    {
        StackWithGetMax<int> stack = new StackWithGetMax<int>();

        public void Tests<T>(T value, StackWithGetMax<T> stackWithGetMax) where T : struct, IComparable<T>
        {
            Assert.AreEqual(value, stackWithGetMax.GetMax());
        }
        
        [TestMethod]

        public void SimpleTest()
        {
            stack.Push(1);
            stack.Push(2);
            stack.Push(3);
            stack.Pop();
            Tests(2, stack);
        }
        
        [TestMethod]
        public void EmptyStackTest()
        {
            stack.Push(1);
            stack.Push(2);
            stack.Push(3);
            stack.Pop();
            stack.Pop();
            stack.Pop();
            Tests(0, stack);
        }
        
        [TestMethod]
        public void TwoPopsAfterTwoPushOfMax()
        {
            stack.Push(1);
            stack.Push(2);
            stack.Push(3);
            stack.Push(3);
            stack.Pop();
            stack.Pop();
            Tests(2, stack);
        }
        
        [TestMethod]
        public void DifficultTest()
        {
            stack.Push(1);
            stack.Push(2);
            stack.Push(3);
            stack.Push(3);
            stack.Push(2);
            stack.Pop();
            stack.Pop();
            Tests(3, stack);
        }
    }
}