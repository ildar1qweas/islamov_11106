﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace GetMaxElementOfStack
{
    public class MyList<T> : IEnumerable<T> 
    {
        private T[] myCollection;

        public int Capacity => myCollection.Length;
        public int Count { get; private set; } 

        public MyList()
        {
            myCollection = new T[100];
        }

        private void Enlarge()
        {
            Array.Resize(ref myCollection, myCollection.Length == 0 ? 1 : myCollection.Length * 2);
        }

        public void Add(T value)
        {
            if(Count == myCollection.Length)
                Enlarge();
            myCollection[Count++] = value;
        }

        public void RemoveAt(int index)
        {
            if (index < 0 || index >= Count)
            {
                throw new IndexOutOfRangeException(nameof(index));
            }
            
            for(int i = index; i < Count - 1; i++)
            {
                myCollection[i] = myCollection[i + 1];
            }
            myCollection[Count - 1] = default(T);
            Count--;
        }
        public IEnumerator<T> GetEnumerator()
        {
            for (int i = 0; i < Count; i++)
                yield return myCollection[i];
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public T this[int index]
        {
            get
            {
                if (index < 0 || index >= Count) throw new ArgumentOutOfRangeException();
                return myCollection[index];
            }
            
            set 
            { 
                if (index < 0 || index >= Count) throw new ArgumentOutOfRangeException(); 
                myCollection[index] = value;
            }
        }
    }
}