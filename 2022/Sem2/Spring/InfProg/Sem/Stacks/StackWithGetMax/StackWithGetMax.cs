﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GetMaxElementOfStack

{
    public class MaxItems<T> where T: struct, IComparable<T>
    {
        protected internal T Value { get; set; }
        protected internal int CountOfMaxValue { get; set; }
    }

    public class StackWithGetMax<T> where T : struct, IComparable<T>

    {
        private MyList<T> listOfValues;
        private MyList<MaxItems<T>> listOfMaxValues;

        public StackWithGetMax()
        {
            listOfValues = new MyList<T>();
            listOfMaxValues = new MyList<MaxItems<T>>();
        }

        public void Push(T value)
        {
            int lastIndex = listOfMaxValues.Count - 1 ;
            listOfValues.Add(value);
            
            if(listOfMaxValues.Count == 0)
                listOfMaxValues.Add(new MaxItems<T> {Value = value, CountOfMaxValue = 1});
            
            else
            {
                if (value.CompareTo(listOfMaxValues[lastIndex].Value) > 0)
                    listOfMaxValues.Add(new MaxItems<T> {Value = value, CountOfMaxValue = 1});
            
                if (value.CompareTo(listOfMaxValues[lastIndex].Value) == 0)
                    listOfMaxValues[lastIndex].CountOfMaxValue++;
            }
            
            
        }

        public T Pop()
        {
            int lastIndex = listOfMaxValues.Count - 1;
            if (listOfValues.Count == 0) throw new InvalidOperationException();
            
            if(listOfMaxValues[lastIndex].CountOfMaxValue == 1) 
                listOfMaxValues.RemoveAt(lastIndex);
            
            else if (listOfValues[listOfValues.Count - 1].Equals(listOfMaxValues[lastIndex].Value))
            {
                listOfMaxValues[lastIndex].CountOfMaxValue--;
            }
            
            var result = listOfValues[listOfValues.Count - 1];
            listOfValues.RemoveAt(listOfValues.Count - 1);
            return result;
        }

        public T GetMax()
        {
            if(listOfMaxValues.Count > 0)
                return listOfMaxValues[listOfMaxValues.Count - 1].Value;
            return default(T);
        }
    }
}