using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;

namespace LINQ
{ 
    class Program
    {
        static void MainX(string[] args)
        {
            string[] str = new[] {"sds", "fdf","", "dfd"};
            var list = str
                .Select((b, index) => Tuple.Create(b, ++index))
                .Where(tuple => tuple.Item1 != String.Empty)
                .Select(tuple => tuple.Item1 + tuple.Item2)
                .ToList();
        }
    }
}