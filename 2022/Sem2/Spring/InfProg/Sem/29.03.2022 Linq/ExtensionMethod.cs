using System;
using System.Collections.Generic;

namespace LINQ
{
    public static class LinqExtension
    {
        public static IEnumerable<Tuple<TResult1,TResult2>> CreateTuples<TItem1, TItem2,TResult1,TResult2>(IEnumerable<TItem1> col1,
            IEnumerable<TItem2> col2, Func<TItem1, TItem2, bool> predicate, 
            Func<TItem1, TResult1> selector1, Func<TItem2, TResult2> selector2)
        {
            foreach (var item1 in col1)
            {
                foreach (var item2 in col2)
                {
                    if (predicate(item1, item2)) yield return Tuple.Create(selector1(item1), selector2(item2));
                }
            }
        }
    }
}