using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QueueWithTwoStacks;

namespace QueueTests
{
    [TestClass]
    public class UnitTest1
    {
        private MyQueue<int> myQueue = new MyQueue<int>();
            
        [TestMethod]
        public void SimpleTest()
        {
            myQueue.Enqueue(1);
            myQueue.Enqueue(2);
            myQueue.Enqueue(3);
            Assert.AreEqual(1,myQueue.Dequeue());
        }
        
        [TestMethod]
        public void GetLastElementOfQueueTest()
        {
            myQueue.Enqueue(1);
            myQueue.Enqueue(2);
            myQueue.Enqueue(3);
            myQueue.Dequeue();
            myQueue.Dequeue();
            Assert.AreEqual(3,myQueue.Dequeue());
        }
    }
}