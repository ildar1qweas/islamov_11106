﻿using System;
using System.Collections.Generic;

namespace QueueWithTwoStacks
{
    public class MyQueue<T>
    {
        private Stack<T> invertedStack;
        private Stack<T> originalStack;

        public MyQueue()
        {
            originalStack = new Stack<T>();
            invertedStack = new Stack<T>();
        }

        public void Enqueue(T value)
        {
            originalStack.Push(value);
        }

        public T Dequeue()
        {
            if (originalStack.Count == 0) throw new InvalidOperationException();
            
            invertedStack = new Stack<T>(originalStack);
            var result = invertedStack.Pop();
            originalStack = new Stack<T>(invertedStack);
            return result;
        }
    }
}