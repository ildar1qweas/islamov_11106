﻿using System.Collections.Generic;

namespace Take
{
    public static class ExtensionTake
    {
        public static IEnumerable<T> Take<T>(this IEnumerable<T> enumerable, int number)
        {
            var enumerator = enumerable.GetEnumerator();
            for (int i = 0; i < number; i++)
            {
                enumerator.MoveNext();
                yield return enumerator.Current;
            }
        }
    }
}