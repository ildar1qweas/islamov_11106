﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace Sequence
{
    public class Sequence :  IEnumerable<int>
    {
        public static IEnumerable<int> NaturalNumber()
        {
            int number = 0;
            while (true)
            {
                yield return ++number;
            }
        }

        public IEnumerator<int> GetEnumerator()
        {
            return new SequenceEnumerator();  
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
    
    public class SequenceEnumerator: IEnumerator<int>
    {
        int currentValue = 0;
        public bool MoveNext()
        {
            currentValue++;
            return true;
        }

        public void Reset()
        {
            
        }
        
        public int Current { get => currentValue; }

        object IEnumerator.Current => Current;

        public void Dispose()
        {
            
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Sequence sequence = new Sequence();
            foreach (var e in sequence)
            {
                Console.WriteLine(e);
                Thread.Sleep(100);
                if(Console.KeyAvailable) break;
            }
        }
    }
}