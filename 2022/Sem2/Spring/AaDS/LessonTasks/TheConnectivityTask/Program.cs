﻿using System;

namespace TheConnectivityTask
{
    class Program
    {
      private const int n = 10000;
              
        static void Main(string[] args)
        {
            var arr = new[]
            {
                (3, 4),
                (4, 9),
                (8, 0),
                (2, 3),
                (5, 6),
                (2, 9),
                (5,9),
                (7,3),
                (4,8),
                (5,6),
                (0,2),
                (6,1)
            };

            QuickUnion quickUnion = new QuickUnion();
            foreach (var tuple in arr)
            {
                quickUnion.Union(tuple.Item1,tuple.Item2);
            }
            
        }
    }
}