﻿using System;
using System.Collections.Generic;

namespace TheConnectivityTask
{
    public class QuickUnion
    {
        private Dictionary<int, (int Parent, int Deep)> tree;
        
        public QuickUnion()
        {
            tree = new Dictionary<int, (int, int)>();
        }

        public int FindOrigin(int k)
        {
            var origin = k;
            while (origin != tree[origin].Parent)
            {
                origin = tree[origin].Parent;
                tree[origin] = tree[tree[origin].Parent];
            }
            return origin;
        }

        public (int, int) Find(int p, int q)
        {
            return (FindOrigin(p), FindOrigin(q));
        }

        public void Union(int p, int q)
        {
            Fill(p);
            Fill(q);
            var origin = Find(p, q);
            var originOfFirst = origin.Item1;
            var originOfSecond = origin.Item2;
            var areConnected = false;

            if (originOfFirst != originOfSecond)
            {
                if (tree[originOfFirst].Deep <= tree[originOfSecond].Deep)
                {
                    var tuple = (originOfFirst, tree[originOfFirst].Deep + tree[originOfSecond].Deep);
                    tree[originOfSecond] = tuple;
                    areConnected = true;
                }

                if (tree[originOfSecond].Deep < tree[originOfFirst].Deep)
                {
                    var tuple = (originOfSecond, tree[originOfFirst].Deep + tree[originOfSecond].Deep);
                    tree[originOfFirst] = tuple;
                    areConnected = true;
                }
            }
            
            if(areConnected)
                Console.WriteLine((p,q));
            else Console.WriteLine();
        }

        public void Fill(int p)
        {
            if(!tree.ContainsKey(p))
                tree.Add(p,(Parent:p,Deep:1));
        }
    }
}