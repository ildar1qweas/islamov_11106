﻿using System;
using System.IO;
using System.Net;
using Microsoft.VisualBasic;

namespace CNF
{
    public class Cnf
    {
        private MyLinkedList ListOfDisjunctions { get; set; }
        private string text;
        private string path;
        public int Length => ListOfDisjunctions.Count;
        
        /// <summary>
        /// Метод индексатор сделан для того, чтобы получать доступ по индексу через сам объект КНФ,а не через список.
        /// Поэтому список - приватное поле.
        /// </summary>
        /// <param name="index">Индекс элемента, который нужно получить.</param>
        /// <exception cref="IndexOutOfRangeException"></exception>
        public int[] this[int index] // O(1)
        {
            get
            {
                if (index >= 0 && index < Length)
                {
                    return ListOfDisjunctions[index];
                }
                throw new IndexOutOfRangeException();
            }
        }

        public Cnf(string textOfFile, string pathOfFile)
        {
            text = textOfFile;
            path = pathOfFile;
            ListOfDisjunctions = new MyLinkedList();
        }

        public Cnf()
        {
            ListOfDisjunctions = new MyLinkedList();
        }

        /// <summary>
        /// Contains возвращает одноимённый метод списка, чтобы не было необходимости работать с полем списка напрямую
        /// </summary>
        /// <param name="disjunct"></param>
        /// <returns></returns>
        public bool Contains(int[] disjunct) // O(n)
        {
            return ListOfDisjunctions.Contains(disjunct);
        }
        /// <summary>
        /// Строит список, состоящий из массивов, где каждый массив - это дизъюнкт.
        /// Список строится по КНФ, заданной в некотором файле.
        /// </summary>
        public void Encode() // O(n)
        {
            var arrayOfDisjuncts = text.Split(new []{'∧', ' ', '(', ')'},StringSplitOptions.RemoveEmptyEntries);
            var arrayOfConjuncts = new string[arrayOfDisjuncts.Length][];
            for (int i = 0; i < arrayOfDisjuncts.Length; i++)
                arrayOfConjuncts[i] = arrayOfDisjuncts[i].Split('∨');
            for (int j = 0; j < arrayOfConjuncts.Length; j++)
            {
                ListOfDisjunctions.Insert(new []{0,0,0,0},j);
                for (int k = 0; k < arrayOfConjuncts[j].Length; k++)
                {
                    // Рассматриваю первый с конца символ, так как у меня строка может состоять из 2 символов(Например: ¬x)
                    // Взял вместо символа t - w, так как код символа t делится без остатка на 4, а w -делится с остатком равным 3.
                    // Т.е x в каждом массиве соответствует первый элемент, y - второй, z - третий, w - четвёртый
                    var byteValue = (byte) arrayOfConjuncts[j][k][^1] % 4;
                    
                    ListOfDisjunctions[j][byteValue] = 1;
                    
                    //Если длина строки равна 2, то это значит, что переменная с отрицанием; отрицание задаётся как -1
                    if (arrayOfConjuncts[j][k].Length == 2)
                        ListOfDisjunctions[j][byteValue] *= -1;
                }
            }
        }

        /// <summary>
        /// Восстанавливает КНФ с выводом результата в текстовый файл, с освобождением выделенной динамической памяти
        /// </summary>
        public void Decode() // O(n)
        {
            string decodingCnf;
            var arrayOfLiterals = new [] {"x", "y", "z", "w"};
            var arrayOfDisjuncts = new string[Length];
            for (int i = 0; i < Length; i++)
            {
                var disjunct = new string[4];
                for (int j = 0; j < 4; j++)
                {
                    if (ListOfDisjunctions[i][j] == 1)
                        disjunct[j] = arrayOfLiterals[j];

                    else if (ListOfDisjunctions[i][j] == -1)
                        disjunct[j] = "¬" + arrayOfLiterals[j];
                }
                
                //Сделал так, поскольку не знал, как избавиться от null при объединении
                var disjJoin = String.Join(" ",disjunct);
                var disjunctOfLiterals = disjJoin.Split(' ', StringSplitOptions.RemoveEmptyEntries);
                arrayOfDisjuncts[i] = String.Join("∨", disjunctOfLiterals);
            }

            decodingCnf = String.Join(" ∧ ", arrayOfDisjuncts);
            File.WriteAllText(path, decodingCnf);
            ListOfDisjunctions.Clear();
        }
        
        /// <summary>
        /// Вставляет дизъюнкт в некоторую позицию по индексу, при этом учитывается,
        /// присутствует этот дизъюнкт в списке или нет.
        /// </summary>
        /// <param name="disjunct">Дизъюнкт; задаётся в параметре метода строкой</param>
        /// <param name="index">Индекс позиции</param>
        public void Insert(string disjunct, int index) //O(n)
        {
            var arrayOfConjuncts = disjunct.Split(new []{'∨', ' ', '(', ')'}, StringSplitOptions.RemoveEmptyEntries);
            var arrayOfLiterals = new []{0,0,0,0};
            for (int i = 0; i < arrayOfConjuncts.Length; i++)
            {
                var byteValue = (byte) arrayOfConjuncts[i][^1] % 4;
                arrayOfLiterals[byteValue] = 1;
                //Если длина строки равна 2, то это значит, что переменная с отрицанием; отрицание задаётся как -1
                if (arrayOfConjuncts[i].Length == 2)
                    arrayOfLiterals[byteValue] *= -1;
            }
            if(!ListOfDisjunctions.Contains(arrayOfLiterals))
                ListOfDisjunctions.Insert(arrayOfLiterals, index);
        }

        /// <summary>
        /// Удаляет элемент, находящийся в j позиции
        /// </summary>
        /// <param name="index">Индекс j позиции</param>
        public void Delete(int index) //O(n)
        {
            ListOfDisjunctions.Remove(index);
        }

        /// <summary>
        /// Объединяет две КНФ в одну, при этом проверяет встречался ли i-ый дизъюнкт до этого.
        /// </summary>
        /// <param name="firstCnf">Первая КНФ.</param>
        /// <param name="secondCnf">Вторая КНФ.</param>
        /// <returns>Возвращает конъюнкцию двух КНФ.</returns>
        public static Cnf UnionTwoCnf(Cnf firstCnf, Cnf secondCnf) //O(n^2)
        {
            var newCnf = new Cnf();
            for(int i = 0; i < firstCnf.Length; i++)
                if(!newCnf.ListOfDisjunctions.Contains(firstCnf[i]))
                    newCnf.ListOfDisjunctions.AddLast(firstCnf[i]);
            for (int j = 0; j < secondCnf.Length; j++)
                if(!newCnf.ListOfDisjunctions.Contains(secondCnf[j]))
                    newCnf.ListOfDisjunctions.AddLast(secondCnf[j]);
            return newCnf;
        }

        /// <summary>
        /// Вычисляет значение КНФ в некоторой вершине четырёхмерного куба.
        /// </summary>
        /// <param name="values">Значения некоторой вершины четырёхмерного куба.</param>
        /// <returns>Возвращает значение КНФ.</returns>
        public bool FindValueOfCnf(params int[] values) //O(n)
        {
            if (Length == 0) return false;
            for (int i = 0; i < Length; i++)
            {
                var tempValueOfDisjunct = 0;
                for (int j = 0; j < 4; j++)
                {
                    if (values[j] == 1)
                    {
                        if (ListOfDisjunctions[i][j] == 1)
                            tempValueOfDisjunct = 1;
                    }
                    else if(values[j] == 0)
                        if (ListOfDisjunctions[i][j] == -1)
                            tempValueOfDisjunct = 1;
                }

                if (tempValueOfDisjunct == 0) return false;
            }
            return true;
        }
        
        /// <summary>
        /// Составляет новую КНФ, состоящую из дизъюнктов, число переменных в которых не превосходит 2.
        /// </summary>
        /// <returns>Возвращает КНФ.</returns>
        public Cnf CreateNewListOfDisjunctions() //O(n)
        {
            var newCnf = new Cnf();
            for (int i = 0; i < Length; i++)
            {
                int counterOfVariable = 0;
                for (int j = 0; j < 4; j++)
                    if (ListOfDisjunctions[i][j] != 0)
                        counterOfVariable++;
                if(counterOfVariable <= 2)
                    newCnf.ListOfDisjunctions.AddLast(ListOfDisjunctions[i]);
            }
            return newCnf;
        }
        
        /// <summary>
        /// Строит новую КНФ из конъюнкций, содержащих переменные x и у.
        /// </summary>
        /// <returns>Возвращает КНФ.</returns>
        public Cnf CreateNewListOfConjuncts()//O(n)
        {
            var newCnf = new Cnf();
            for (int i = 0; i < Length; i++)
            {
                // Если позиция, на которой расположен x или y - ненулевые, то добавляю в новую КНФ
                bool containsOnlyX = ListOfDisjunctions.ArrayEquals(this[i], new []{1,0,0,0}) 
                                     || ListOfDisjunctions.ArrayEquals(this[i], new []{-1,0,0,0});
                bool containsOnlyY = ListOfDisjunctions.ArrayEquals(this[i], new []{0,1,0,0}) 
                                     || ListOfDisjunctions.ArrayEquals(this[i], new []{0,-1,0,0});
                if(containsOnlyX || containsOnlyY) newCnf.ListOfDisjunctions.AddLast(this[i]);
            }
            return newCnf;
        }
    }
}