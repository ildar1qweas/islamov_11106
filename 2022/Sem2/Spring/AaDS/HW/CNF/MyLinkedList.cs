﻿using System;
using System.Runtime.InteropServices;

namespace CNF
{
    public class MyLinkedList
    {
        public class Node
        {
            public Node(int[] data)
            {
                Data = data;
            }
            public int[] Data { get; set; }
            public Node Next { get; set; }
        }

        private Node head;
        private Node tail;
        public int Count { get; private set; }
        
        
        public int[] this[int index] // O(n)
        {
            get
            {
                if (index >= 0 && index < Count)
                {
                    Node current = head;
                    for (int i = 0; i < index; i++)
                    {
                        if (current.Next == null) throw new IndexOutOfRangeException();
                        current = current.Next;
                    }
                
                    return current.Data;
                }

                throw new IndexOutOfRangeException();
            }

            /*set
            {
                if (index >= 0 && index < Count)
                {
                    Node current = head;
                    for (int i = 0; i < index; i++)
                    {
                        if (current.Next == null) throw new IndexOutOfRangeException();
                        current = current.Next;
                    }

                    current.Data = value;
                }

                else
                    throw new IndexOutOfRangeException();
            }*/
        }

        // Вставка элемента в список
        public void Insert(int[] arr, int index) // O(n) - получение позиции элемента и O(1) - добавление
        {
            if (index < 0 || index > Count) throw new IndexOutOfRangeException();

            if (Contains(arr)) return;
            var node = new Node(arr);
            if (index == 0)
            {
                AddFirst(arr);
                return;
            }
                
            if (index == Count)
            {
                AddLast(arr);
                return;
            }
            
            var current = head;
            for (int i = 1; i < index; i++)
                current = current.Next;
            var temp = current.Next;
            current.Next = node;
            node.Next = temp;   
            
            Count++;
        }

        // Добавение элемента на первую позицию
        private void AddFirst(int[] arr) // O(1)
        {
            Node temp = head;
            Node node = new Node(arr);
            node.Next = temp;
            head = node;
            if (Count == 0)
                tail = head;
            Count++;
        }

        // Добавение элемента на последнюю позицию
        // Метод публичный, поскольку в методе UnionTwoCnf мне нужно добавлять в конец,
        // а для Insert нужен индекс позиции, куда я хочу поставить свой дизъюнкт. 
        public void AddLast(int[] arr) // O(1)
        {
            Node node = new Node(arr);
 
            if (head == null)
                head = node;
            else
                tail.Next = node;
            tail = node;
            Count++;
        }

        //Удаляет элемент 
        public void Remove(int index) // O(n) - получение позиции элемента и O(1) его удаление
        {
            if (head == null) throw new NullReferenceException();
            if (index < 0 || index >= Count) throw new IndexOutOfRangeException();
            if (index == 0)
            {
                RemoveFirst();
                return;
            }
            if (index == Count - 1)
            {
                RemoveLast();
                return;
            }
            var current = head;
            int indexOfCurrent = 0;
            while (current != null)
            {
                if (indexOfCurrent == index - 1)
                {
                    var temp = current.Next.Next;
                    current.Next = temp;
                    break;
                }
            
                current = current.Next;
                indexOfCurrent++;
            }
            Count--;
            if(Count == 0) 
                Clear();
        }

        private void RemoveLast() // O(n) - получение позиции последнего элемента и O(1) его удаление
        {
            var current = head;
            for(int i = 1; i < Count - 1; i++)
                current = current.Next;

            tail = current;
            current.Next = null;
            Count--;
            if(Count == 0) 
                Clear();
        }

        private void RemoveFirst() // O(1)
        {
            if (head != null)
                head = head.Next;
            Count--;
            if(Count == 0) 
                Clear();
        }

        public bool Contains(int[] arr) // O(n)
        {
            Node current = head;
            while (current != null)
            {
                if (ArrayEquals(arr, current.Data))
                    return true;
                current = current.Next;
            }

            return false;
        }

        //Проверка массивов длины 4 на равенство
        public bool ArrayEquals(int[] arr1, int[] arr2) // O(1)
        {
            if (arr1.Length == arr2.Length)
            {
                for (int i = 0; i < arr1.Length; i++)
                    if (arr1[i] != arr2[i]) 
                        return false;
                return true;
            }
            return false;
        }
        
        // Освобождение выделенной динамической памяти
        public void Clear() // O(1)
        {
            head = null;
            tail = null;
            Count = 0;
        }
    }
}