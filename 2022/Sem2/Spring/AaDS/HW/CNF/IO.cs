﻿using System.IO;
using System.Net;

namespace CNF
{
    public class IO
    {
        public string ReadTextFromFile(string path)
        {
            return File.ReadAllText(path);
        }

        public void WriteCnfToFile(string path, string cnf)
        { 
            File.WriteAllText(path, cnf);
        }
    }
}