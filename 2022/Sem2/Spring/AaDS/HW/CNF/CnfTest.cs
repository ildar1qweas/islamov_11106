using System;
using System.Collections.Immutable;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CNF;

namespace CnfTests
{
    [TestClass]
    public class UnitTest1
    {
        private IO IO = new IO();
        [TestMethod]
        public void EncodeTest()
        {
            var path = @"C:\Users\Ильдар\Desktop\HomeWork\CNF\CNF\Cnf.txt";
            var text = IO.ReadTextFromFile(path);
            Cnf cnf = new Cnf(text,path);
            cnf.Encode();
            MyLinkedList list = new MyLinkedList();
            list.AddLast(new[] {0, 1, 0, -1});
            list.AddLast(new[] {1, 1, 1, 0});
            list.AddLast(new[] {-1, 0, 0, 1});
            list.AddLast(new[] {0, 1, 0, 1});
            list.AddLast(new[] {0, 0, 1, -1});
            list.AddLast(new[] {-1, 1, -1, 1});
            list.AddLast(new[] {1, -1, 1, -1});
            list.AddLast(new[] {-1, 1, 1, 1});
            list.AddLast(new[] {1, 1, -1, 1});
            list.AddLast(new []{1, -1, 1, 1});
            for (int i = 0; i < cnf.Length; i++)
            {
                CollectionAssert.AreEqual(cnf[i],list[i]);
            }
        }
        
        [TestMethod]
        public void InsertTest()
        {
            var path = @"C:\Users\Ильдар\Desktop\HomeWork\CNF\CNF\Cnf.txt";
            var text = IO.ReadTextFromFile(path);
            Cnf cnf = new Cnf(text,path);
            cnf.Encode();
            cnf.Insert("(x∨z∨¬w)", 2);
            var disjunction = new[] {1, 0, 1, -1};
            CollectionAssert.AreEqual(cnf[2], disjunction);
            
        }

        [TestMethod]
        public void InsertFirstTest()
        {
            var path = @"C:\Users\Ильдар\Desktop\HomeWork\CNF\CNF\Cnf.txt";
            var text = IO.ReadTextFromFile(path);
            Cnf cnf = new Cnf(text,path);
            cnf.Encode();
            cnf.Insert("(y∨¬z∨w)", 0);
            var disjunction = new[] {0, 1, -1, 1};
            CollectionAssert.AreEqual(cnf[0], disjunction);
        }
        

        [TestMethod]
        public void DeleteTest()
        {
            var path = @"C:\Users\Ильдар\Desktop\HomeWork\CNF\CNF\Cnf.txt";
            var text = IO.ReadTextFromFile(path);
            Cnf cnf = new Cnf(text,path);
            cnf.Encode();
            cnf.Delete(1);
            Assert.IsTrue(!cnf.Contains(new []{0,1,1,1}));
        }

        [TestMethod]
        public void DeleteFromEmptyList()
        {
            var path = @"C:\Users\Ильдар\Desktop\HomeWork\CNF\CNF\Cnf.txt";
            var text = IO.ReadTextFromFile(path);
            Cnf emptyCnf = new Cnf(text,path);
            Assert.ThrowsException<NullReferenceException>(() => emptyCnf.Delete(0));
        }

        [TestMethod]
        public void InsertToEmptyList()
        {
            var path = @"C:\Users\Ильдар\Desktop\HomeWork\CNF\CNF\Cnf.txt";
            var text = IO.ReadTextFromFile(path);
            Cnf emptyCnf = new Cnf(text,path);
            emptyCnf.Insert("x∨y∨w", 0);
            CollectionAssert.AreEqual(emptyCnf[0],new[]{1,1,0,1});
        }

        [TestMethod]
        public void CreateNewListOfDisjunctionsTest()
        {
            var path = @"C:\Users\Ильдар\Desktop\HomeWork\CNF\CNF\SecondCnf.txt";
            var text = IO.ReadTextFromFile(path);
            Cnf secondCnf = new Cnf(text,path);
            secondCnf.Encode();
            Assert.AreEqual(secondCnf.CreateNewListOfDisjunctions().Length,8);
        }

        [TestMethod]
        public void CreateNewListOfConjunctsTest()
        {
            var path = @"C:\Users\Ильдар\Desktop\HomeWork\CNF\CNF\SecondCnf.txt";
            var text = IO.ReadTextFromFile(path);
            Cnf secondCnf = new Cnf(text,path);
            secondCnf.Encode();
            Assert.AreEqual(secondCnf.CreateNewListOfConjuncts().Length,2);
        }

        [TestMethod]
        public void FindValueOfCnfTest()
        {
            var arrayOfValues = new[]
            {
                false,false,false,false,false,false,true,true,
                false,false,false,false,false,false,false,true
            };
            var path = @"C:\Users\Ильдар\Desktop\HomeWork\CNF\CNF\Cnf.txt";
            var text = IO.ReadTextFromFile(path);
            Cnf cnf = new Cnf(text,path);
            cnf.Encode();
            var array = new[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            GetGrayCode(4, array, 1);
            Array.Sort(array);
            for (int i = 0; i < 16; i++)
            {
                int[] disjunct = new int[4];
                var valueOfVertex = Convert.ToString(array[i], 2).PadLeft(4,'0').ToCharArray();
                for (int j = 0; j < 4; j++)
                    disjunct[j] = (valueOfVertex[j] - '0');
                Assert.AreEqual(cnf.FindValueOfCnf(disjunct), arrayOfValues[i]);
            } 
                
            
        }
        
        [TestMethod]
        public void FindValueOfNewCnfTest()
        {   
            var arrayOfValues = new[]
            {
                false,false,false,false,false,false,false,true,
                false,false,false,false,false,false,false,false
            };
            var path = @"C:\Users\Ильдар\Desktop\HomeWork\CNF\CNF\SecondCnf.txt";
            var text = IO.ReadTextFromFile(path);
            Cnf secondCnf = new Cnf(text,path);
            secondCnf.Encode();
            var array = new[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            GetGrayCode(4, array, 1);
            Array.Sort(array);
            for (int i = 0; i < 16; i++)
            {
                int[] disjunct = new int[4];
                var valueOfVertex = Convert.ToString(array[i], 2).PadLeft(4,'0').ToCharArray();
                for (int j = 0; j < 4; j++)
                    disjunct[j] = (valueOfVertex[j] - '0');
                Assert.AreEqual(secondCnf.FindValueOfCnf(disjunct), arrayOfValues[i]);
            } 
            
        }
        
        [TestMethod]
        public void UnionTwoCnfTest()
        {   
            var path = @"C:\Users\Ильдар\Desktop\HomeWork\CNF\CNF\Cnf.txt";
            var text = IO.ReadTextFromFile(path);
            Cnf cnf = new Cnf(text,path);
            cnf.Encode();
            var pathOfSecond = @"C:\Users\Ильдар\Desktop\HomeWork\CNF\CNF\SecondCnf.txt";
            var textOfSecond = IO.ReadTextFromFile(pathOfSecond);
            Cnf secondCnf = new Cnf(textOfSecond,pathOfSecond);
            secondCnf.Encode();
            Assert.AreEqual(Cnf.UnionTwoCnf(cnf,secondCnf).Length,14);
        }
        
         public int GetGrayCode(int length, int[] array, int depth)
        {
            int i; 
            int t = (1 << (depth - 1));
            if (depth == 0)
                array[0] = 0;
            else 
            {
                for (i = 0; i < t; i++)
                    array[t + i] = array[t - i - 1] + (1 << (depth - 1));
            }
            if (depth != length)
                GetGrayCode(length, array, depth + 1);
            return 0;
        }
    }
}