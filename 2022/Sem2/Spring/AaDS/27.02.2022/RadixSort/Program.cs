﻿using System;
using System.Collections.Generic;

namespace RadixSort
{
    class Program
    {
        static void Main(string[] args)
        {
            
            char[][] array =
            {
                new char[] {'A', 'B'},
                new char[] {'a', 'b'},
                new char[] {'C', 'c'},
                new char[] {'l', 'M', '0'}
            };

            char[] alphabet = new char[]{' ','0', 'A', 'B', 'C', 'M', 'a', 'b', 'c', 'l'};
            
            WriterForArray(array);
            WriterForQueue(RadixSort(array, 4, alphabet));
        }

        public static void WriterForQueue(MyQueue queue)
        {
            foreach (var item in queue)
            {
                Console.WriteLine(item);
            }
        }
        
        public static void WriterForArray(char[][] array)
        {
            foreach (var item in array)
                Console.WriteLine(item);
        }

        public static MyQueue RadixSort(char[][] charArray, int maxLengthOfWord, char[] alphabet)
        {
            MyQueue queue = new MyQueue();
            Dictionary<char,  MyQueue> dictionary = new Dictionary<char, MyQueue>();
            //MyQueue resultQueue = new MyQueue();
            PrepareForRadix(queue, charArray, maxLengthOfWord, alphabet);

            for (int radix = 1; radix < maxLengthOfWord; radix++)
            {
                dictionary.Clear();
                for (int i = 0; i < alphabet.Length; i++)
                {
                    dictionary.Add(alphabet[i], new MyQueue());
                }
                
                while (!queue.IsEmpty)
                {
                    var result = queue.Dequeue();
                    dictionary[result[^radix]].Enqueue(result);
                }
                
                foreach (var element in dictionary)
                {
                    if(element.Value.IsEmpty) continue;
                    MyQueue.Join(queue, element.Value);
                }
            }
            return queue;
        }

        public static void PrepareForRadix(MyQueue queue, char[][] charArray, int m, char[] alphabet)
        {
            for (int i = 0; i < charArray.Length; i++)
            {
                string str = new string(charArray[i]).PadLeft(m,alphabet[0]);
                queue.Enqueue(str);
            }
        }
    }
}
