﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;


namespace RadixSort
{
    public class QueueItem 
    {
        
        public string Value { get; set; }
        public QueueItem Next { get; set; }
    }

    public class MyQueue : IEnumerable
    {
        private QueueItem Head { get; set; }
        QueueItem tail;
        
        public bool IsEmpty { get { return Head == null; } }
        
        public void Clear()
        {
            Head = null;
            tail = null;
        }

        #region
        public void Enqueue(string value)
        {
            if (IsEmpty)
                tail = Head = new QueueItem { Value = value, Next = null };
            else
            {
                var item = new QueueItem { Value = value, Next = null };
                tail.Next = item;
                tail = item;
            }
        }

        public string Dequeue()
        {
            if (Head == null) throw new InvalidOperationException();
            var result = Head.Value;
            Head = Head.Next;
            if (Head == null)
                tail = null;
            return result;
        }

        public static void Join(MyQueue originalQueue, MyQueue nextMyQueue)
        {
            if (!originalQueue.IsEmpty && !nextMyQueue.IsEmpty)
            {
                originalQueue.tail.Next = nextMyQueue.Head;
                originalQueue.tail = nextMyQueue.tail;
            }
            else if (!nextMyQueue.IsEmpty)
            {
                originalQueue.Head = nextMyQueue.Head;
                originalQueue.tail = nextMyQueue.tail;
            }
            
        }
        
        #endregion
        public IEnumerator GetEnumerator()
        {
            var current = Head;
            while (current != null)
            {
                yield return current.Value;
                current = current.Next;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}