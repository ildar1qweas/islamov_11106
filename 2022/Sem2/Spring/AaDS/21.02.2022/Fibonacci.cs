﻿using System;
using System.Diagnostics;
using System.Threading;

namespace MyFibonacci
{
    class Program
    {
        public delegate long Fibonacci(long value);
        
        public static decimal MeasureDurationInMs(Fibonacci fibonacci , int repetitionCount, long n)
        {
            GC.Collect();                  
            GC.WaitForPendingFinalizers();  
           
            Stopwatch stopwatch = new Stopwatch();
            WarmUpRun(fibonacci, n);
            stopwatch.Start();

            for (var i = 0; i < repetitionCount; i++)
            {
                fibonacci(n);
            }
            stopwatch.Stop();
            return (decimal) stopwatch.ElapsedMilliseconds / repetitionCount;
        }
        
        public static void WarmUpRun(Fibonacci fibonacci, long n)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            fibonacci(n);
            Thread.Sleep(1000);
            stopwatch.Stop();
        }
        
        
        static void Main(string[] args)
        {
            Matrix matrix = new Matrix();

            NoteTime(matrix.MatrixFib, 100);
            
            NoteTime(IterativeFib, 100);
            
            Console.WriteLine("MethodName: {0}, RepetitionCount: {1}, Time: {2},", "RecursiveFib",
                1000, MeasureDurationInMs(RecursiveFib, 1000, 30));
            Console.WriteLine("MethodName: {0}, RepetitionCount: {1}, Time: {2},", "RecursiveFib",
                3000, MeasureDurationInMs(RecursiveFib, 3000, 30));
            
        }

        static void NoteTime(Fibonacci fibonacci, long n)
        {
            int[] array = new[] {5000, 10000, 20000, 50000};
            Console.WriteLine(fibonacci(n));
            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine("MethodName: {0}, RepetitionCount: {1}, Time: {2},", fibonacci.Method.Name,
                    array[i], MeasureDurationInMs(fibonacci, array[i], n));
            }
            Console.WriteLine();
        }

        static long RecursiveFib(long n)
        {
            if (n == 0 || n == 1) return n;
            return RecursiveFib(n - 1) + RecursiveFib(n - 2);
        }

        static long IterativeFib(long n)
        {
            if (n == 0 || n == 1) return n;
            
            long result = 1;
            long b = 2;
            long temp;
            
            for (long i = 2; i < n; i++)
            {
                temp = b + result;
                result = b;
                b = temp;
            }

            return result;
        }

        
        public class Matrix
        {
            private long[,] m = {{1,1},{1,0}};

            public static Matrix operator*(Matrix m1, Matrix m2)
            {
                Matrix matrix = new Matrix();
                matrix.m[0,0] = m1.m[0,0] * m2.m[0,0] + m1.m[0,1] * m2.m[1,0];
                matrix.m[0,1] = m1.m[0,0] * m2.m[0,1] + m1.m[0,1] * m2.m[1,1];
                matrix.m[1,0] = m1.m[1,0] * m2.m[0,0] + m1.m[1,1] * m2.m[1,0];
                matrix.m[1,1] = m1.m[1,0] * m2.m[0,1] + m1.m[1,1] * m2.m[1,1];
                return matrix;
            }

            private Matrix Pow(Matrix matrix, long n)
            {
                if (n == 1) return matrix;
                Matrix matrixFib = Pow(matrix,n/2);
                matrixFib = matrixFib * matrixFib;
                if (n%2 == 1) matrixFib = matrixFib * matrix; 
                return matrixFib;
            }
            
            public long MatrixFib(long n)
            {
                if (n == 0) return 0;
                Matrix matrix = new Matrix();
                var fib = matrix.Pow(matrix, n);
                return fib.m[0,1];
            }
        }
    }
}