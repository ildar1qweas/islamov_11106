﻿using System.Collections.Generic;
using System.IO;

namespace CombSort
{
    public class IO
    {
        public static string[] ReadData(string path)
        {
            return File.ReadAllLines(path);
        }

        public static void WriteData(string path, string[] data)
        {
            File.AppendAllLines(path, data);
        }

        public static void WriteTimeAndIterationsToFile(string path, string data)
        {
            File.AppendAllText(path, data);
        }
    }
}