﻿using System;
using System.Collections.Generic;


namespace CombSort
{
    public class CombSort
    {
        public static ulong CombSortingArray<T>(IList<T> list) where T: IComparable<T>
        {
            ulong countOfIteration = 0;
            bool swapped = true;
            int gap = list.Count;
            double shrinkFactor = 1.24733;
            int p = 0;
            while (gap > 1 || swapped)
            {
                p++;
                swapped = false;
                gap = (int) (gap / shrinkFactor);
                
                // Before each subsequent stroke, the gap is reduced to the previous gap divided by 1.24733;
                // If this quotient becomes less than 1, the gap is reduced to 1, collapsing Combsort into a bubble sort.
                if (gap == 0) 
                    gap = 1;
                
                // All mini-turtles are killed before the gap reaches 1 if gap == 11;
                // If gap == 10 and gap == 9,
                // there is about 8 percent of lists have surviving mini-turtles,
                // causing the routine to slow by 15 percent to 20 percent.
                if (gap == 10 || gap == 9) 
                    gap = 11;

                for (int i = 0; i < list.Count - gap; i++)
                {
                    if (list[i].CompareTo(list[i + gap]) >= 1)
                    {
                        (list[i], list[i + gap]) = (list[i + gap], list[i]);
                        swapped = true;
                        countOfIteration++;
                    }
                }
            }

            return countOfIteration;
        }

        //TODO Можно обобщить методы, можно 2 метода сделать
        public static ulong CombSortingLinkedList<T>(IList<T> list) where T: IComparable<T>
        {
            var myLinkedList = list as MyLinkedList<T>;
            int gap = list.Count;
            ulong countOfIteration = 0;
            double shrinkFactor = 1.24733;
            bool swapped = true;
            while (gap > 1 || swapped)
            {
                swapped = false;
                gap = (int) (gap / shrinkFactor);
                
                // Before each subsequent stroke, the gap is reduced to the previous gap divided by 1.24733;
                // If this quotient becomes less than 1, the gap is reduced to 1, collapsing Combsort into a bubble sort.
                if (gap == 0) 
                    gap = 1;
                
                // All mini-turtles are killed before the gap reaches 1 if gap == 11;
                // If gap == 10 and gap == 9,
                // there is about 8 percent of lists have surviving mini-turtles,
                // causing the routine to slow by 15 percent to 20 percent.
                if (gap == 10 || gap == 9) 
                    gap = 11;

                for (int i = 0; i < myLinkedList?.Count - gap; i++)
                {
                    if (myLinkedList[i].CompareTo(myLinkedList[i + gap]) >= 1)
                    {
                        (myLinkedList[i], myLinkedList[i + gap]) = (myLinkedList[i + gap], myLinkedList[i]);
                        swapped = true;
                        countOfIteration++;
                    }
                }
            }
           
            return myLinkedList.Iterations + countOfIteration; 
        }
    }
}