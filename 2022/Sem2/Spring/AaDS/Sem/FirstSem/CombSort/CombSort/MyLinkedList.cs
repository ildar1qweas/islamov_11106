﻿using System;
using System.Collections;
using System.Collections.Generic;


namespace CombSort
{
    public class MyLinkedList<T> : IList<T> where T : IComparable<T>
    {
        //TODO перепиши свой лист под ICollection<T>
        public class Node<T>
        {
            public Node(T data)
            {
                Data = data;
            }

            public T Data { get; set; }
            public Node<T> Next { get; set; }
        }

        private Node<T> head;
        private Node<T> tail;

        private ulong iterations;
        public ulong Iterations => iterations;
        public int Count { get; private set; }
        public bool IsReadOnly { get { return false; } }
        
        public T this[int index] // O(n)
        {
            get
            {
                if (index >= 0 && index < Count)
                {
                    Node<T> current = head;
                    for (int i = 0; i < index; i++)
                    {
                        if (current.Next == null) throw new IndexOutOfRangeException();
                        current = current.Next;
                        iterations++;
                    }

                    return current.Data;
                }
                
                throw new IndexOutOfRangeException();
            }

            set
            {
                if (index >= 0 && index < Count)
                {
                    Node<T> current = head;
                    for (int i = 0; i < index; i++)
                    {
                        if (current.Next == null) throw new IndexOutOfRangeException();
                        current = current.Next;
                        iterations++;
                    }

                    current.Data = value;
                }

                else
                    throw new IndexOutOfRangeException();
            }
        }
        
        public int IndexOf(T item)
        {
            throw new NotImplementedException();
        }

        public void Insert(int index, T value)
        {
            if (index < 0 || index > Count) throw new IndexOutOfRangeException();
            
            var node = new Node<T>(value);
            if (index == 0)
            {
                AddFirst(value);
                return;
            }
                
            if (index == Count)
            {
                Add(value);
                return;
            }
            
            var current = head;
            for (int i = 1; i < index; i++)
                current = current.Next;
            var temp = current.Next;
            current.Next = node;
            node.Next = temp;   
            
            Count++;
        }

        
        public void RemoveAt(int index) 
        {
            if (head == null) throw new NullReferenceException();
            if (index < 0 || index >= Count) throw new IndexOutOfRangeException();
            if (index == 0)
            {
                RemoveFirst();
                return;
            }
            if (index == Count - 1)
            {
                RemoveLast();
                return;
            }
            var current = head;
            int indexOfCurrent = 0;
            while (current != null)
            {
                if (indexOfCurrent == index - 1)
                {
                    var temp = current.Next.Next;
                    current.Next = temp;
                    break;
                }
            
                current = current.Next;
                indexOfCurrent++;
            }
            Count--;
            if(Count == 0) 
                Clear();
        }
        
        private void RemoveLast() 
        {
            var current = head;
            for(int i = 1; i < Count - 1; i++)
                current = current.Next;

            tail = current;
            current.Next = null;
            Count--;
            if(Count == 0) 
                Clear();
        }

        private void RemoveFirst() 
        {
            if (head != null)
                head = head.Next;
            Count--;
            if(Count == 0) 
                Clear();
        }
        
        private void AddFirst(T value) // O(1)
        {
            Node<T> temp = head;
            Node<T> node = new Node<T>(value);
            node.Next = temp;
            head = node;
            if (Count == 0)
                tail = head;
            Count++;
        }
        
        public void Add(T? value) // O(1)
        {
            Node<T> node = new Node<T>(value);

            if (head == null)
                head = node;
            else
                tail.Next = node;
            tail = node;
            Count++;
        }
        
        public bool Remove(T? data)
        {
            Node<T> current = head;
            Node<T> previous = null;
 
            while (current != null)
            {
                if (current.Data.Equals(data))
                {
                    if (previous != null)
                    {
                        previous.Next = current.Next;
                        if (current.Next == null)
                            tail = previous;
                    }
                    else
                    {
                        head = head.Next;
                        if (head == null)
                            tail = null;
                    }
                    Count--;
                    return true;
                }
                previous = current;
                current = current.Next;
            }
            return false;
        }

        public void Clear()
        {
            head = null;
            tail = null;
            Count = 0;
        }

        public bool Contains(T? item)
        {
            Node<T> current = head;
            while (current != null)
            {
                if (current.Data.Equals(item))
                    return true;
                current = current.Next;
            }

            return false;
        }

        public void CopyTo(T[]? array, int arrayIndex)
        {
            throw new NotImplementedException();
        }
        
        public IEnumerator<T> GetEnumerator()
        {
            Node<T> current = head;
            while (current != null)
            {
                yield return current.Data;
                current = current.Next;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}