﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace CombSort
{
    public class Calculator
    {
        public static (ulong, double) CalculateTimeAndIterationsForArray<T>(T[][] array, Func<IList<T>, ulong> sortProcedure) 
            where T : IComparable<T>
        {
            ulong iterations = 0;
            var watch = new Stopwatch();
            GC.Collect();
            GC.WaitForPendingFinalizers();
            watch.Start();
            for (int i = 0; i < 50; i++)
                iterations += sortProcedure(array[i]);
            watch.Stop();
            return (iterations / 50,  watch.Elapsed.TotalMilliseconds / 50);
        }
		
        public static (ulong, double) CalculateTimeAndIterationsForLinkedList<T>(T[][] array, Func<IList<T>, ulong> sortProcedure) 
            where T : IComparable<T>
        {
            ulong iterations = 0;
            var watch = new Stopwatch();
            GC.Collect();
            GC.WaitForPendingFinalizers();
            watch.Start();
            for (int i = 0; i < 50; i++)
            {
                MyLinkedList<T> list;
                list = DataConverter.ConvertArrayDataToLinkedList(array[i]);
                iterations += sortProcedure(list);
                list.Clear();
            }
            watch.Stop();
            return (iterations / 50,  watch.Elapsed.TotalMilliseconds / 50);
        }
    }
}