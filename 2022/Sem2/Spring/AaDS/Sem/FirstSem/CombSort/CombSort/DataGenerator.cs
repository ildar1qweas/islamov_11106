﻿using System;
using System.Text;

namespace CombSort
{
    public class DataGenerator
    {
        private static string[] GenerateRandomData(int length)
        {

            var result = new string[length];
            var rnd = new Random();
            for (int i = 0; i < length; i++)
                result[i] = rnd.Next().ToString();
            return result;
        }

        private static void GenerateData()
        {
            var countOfDataValues = 100;
            for (int i = 1; i < 101; i++)
            {
                for (int j = 0; j < 50; j++)
                {
                    IO.WriteData($@"C:\Users\Ильдар\Desktop\Data\{i}.txt",GenerateRandomData(countOfDataValues));
                    IO.WriteData($@"C:\Users\Ильдар\Desktop\Data\{i}.txt", new []{""});
                }

                countOfDataValues += 100;
            }
        }
    }
}