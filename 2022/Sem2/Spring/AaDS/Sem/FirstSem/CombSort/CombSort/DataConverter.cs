﻿using System;

namespace CombSort
{
    public class DataConverter
    {
        public static int[][] ConvertDataFromFile(int numberOfFile)
        {
            var data = IO.ReadData($@"C:\Users\Ильдар\Desktop\Data\{numberOfFile}.txt");
            var intData = new int[50][];
            for (int i = 0; i < intData.Length; i++)
                intData[i] = new int[100 * numberOfFile];
            var numberOfArray = 0;
            var numberOfElement = 0;
            for(int i = 0; i < data.Length;i++)
            {
                if (numberOfElement == 100 * numberOfFile)
                {
                    numberOfElement = 0;    
                    numberOfArray++;
                    continue;
                }
                
                intData[numberOfArray][numberOfElement] = Convert.ToInt32(data[i]);
                numberOfElement++;
            }
            
            return intData;
        }
        
        public static MyLinkedList<T> ConvertArrayDataToLinkedList<T>(T[] array) where T: IComparable<T>
        {
            MyLinkedList<T> list = new MyLinkedList<T>();
            foreach (var value in array)
            {
                list.Add(value);
            }
			
            return list;
        }
    }
}