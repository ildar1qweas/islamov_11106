﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace CombSort
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 1; i < 101; i++)
            {
                var array = DataConverter.ConvertDataFromFile(i);
                //var result = Calculator.CalculateTimeAndIterationsForLinkedList(array, CombSort.CombSortingLinkedList);
                var result = Calculator.CalculateTimeAndIterationsForArray(array, CombSort.CombSortingArray);
                var iterations = result.Item1;
                var milliseconds = result.Item2;
                IO.WriteTimeAndIterationsToFile(@"C:\Users\Ильдар\Desktop\Data\ArrayData2.txt",$"{i*100} {iterations} {milliseconds}\n");
                
            }
        }
    }
}