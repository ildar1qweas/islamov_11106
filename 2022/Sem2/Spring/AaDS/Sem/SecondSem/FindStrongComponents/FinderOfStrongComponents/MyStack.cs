﻿
using System;
using System.Collections;
using System.Collections.Generic;

namespace FinderOfStrongComponents
{
    public class MyStack<T>
    {
        private class StackItem<T>
        {
            public StackItem(T value)
            {
                Value = value;
            }
			
            public T Value { get;}
            public StackItem<T> Next { get; set; }
        }
			
        private StackItem<T> Head { get; set; }
        public bool IsEmpty => Head == null;

        public int Count { get; private set; }
			
        public MyStack(MyStack<T> previous)
        {
            Head = previous.Head;
        }

        public MyStack() { }

        public void Push(T value)
        {
            var stackItem = new StackItem<T>(value);
            stackItem.Next = Head;
            Head = stackItem;
            Count++;
        }

        public T Pop()
        {
            if (IsEmpty) throw new ArgumentOutOfRangeException();
            StackItem<T> temple = Head;
            Head = Head.Next;
            Count--;
            return temple.Value;
        }

        public T Peek()
        {
            if (IsEmpty) throw new ArgumentOutOfRangeException();
            return Head.Value;
        }

        public void Clean()
        {
            Head = null;
            Count = 0;
        }
        
    }
}