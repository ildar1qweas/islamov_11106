﻿using System;
using System.Collections.Generic;

namespace FinderOfStrongComponents
{
    public class Searcher
    {
        private static int Index { get; set; } = 1;

        private static MyStack<Graph.Node> stack;

        private static List<List<Graph.Node>> listOfComponents;

        private static Graph graphForAlgorithm;
        
        public static List<List<Graph.Node>> SearchStrongComponents(Graph graph, List<Graph.Node> listOfNodes)
        {
            graphForAlgorithm = graph;
            if (graph.IsEmpty) return new List<List<Graph.Node>>();
            stack = new MyStack<Graph.Node>();
            listOfComponents = new List<List<Graph.Node>>();
            if (graph.IsEmpty) throw new ArgumentOutOfRangeException();
            foreach (var node in listOfNodes)
            {
                if (!node.Visited)
                {
                    DepthSearch(node);
                    stack.Clean();
                }
            }
            return listOfComponents;
        }

        //O(|V| + |E|)
        private static void DepthSearch(Graph.Node node)
        {
            node.Visited = true;
            node.IndexInDfs = Index;
            node.Lowlink = Index;
            Index++;
            stack.Push(node);
            node.OnStack = true;
            if (graphForAlgorithm[node.NumberOfNode - 1].Count != 0)
            {
                foreach (var otherNode in graphForAlgorithm[node.NumberOfNode - 1])
                {
                    if (!otherNode.Visited)
                    {
                        DepthSearch(otherNode);
                        node.Lowlink = Math.Min(node.Lowlink, otherNode.Lowlink);
                    }
                    //Если вершина в стеке, значит она принадлежит текущей компоненте, иначе нет
                    else if (otherNode.OnStack)
                        node.Lowlink = Math.Min(node.Lowlink, otherNode.IndexInDfs);
                }   
            }

            if (node.Lowlink == node.IndexInDfs)
            {
                int tempNumber;
                var component = new List<Graph.Node>();
                do
                {
                    var otherNode = stack.Pop();
                    tempNumber = otherNode.NumberOfNode;
                    otherNode.OnStack = false;
                    component.Add(otherNode);
                } while (tempNumber != node.NumberOfNode);
                
                listOfComponents.Add(component);
            }
        }
    }
}