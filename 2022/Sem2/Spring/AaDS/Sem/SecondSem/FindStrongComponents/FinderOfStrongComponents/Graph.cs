﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FinderOfStrongComponents
{
    
    
    public class Graph
    {
        public class Node
        {
            public int Lowlink { get; set; }

            public bool OnStack { get; set; }
        
            public int IndexInDfs { get; set; }
        
            public int NumberOfNode { get; }
        
            public bool Visited { get; set; }

            public Node(int numberOfNode)
            {
                NumberOfNode = numberOfNode;
                Visited = false;
            }
        }
        
        private List<List<Node>> adjacencyLists;

        public bool IsEmpty => adjacencyLists.Count == 0;

        public Graph(int size)
        {
            adjacencyLists = new List<List<Node>>();

            for (int i = 0; i < size; i++)
                adjacencyLists.Add(new List<Node>());
        }

        private void AddEdge(int numberOfNode, Node incidentNode)
        {
            adjacencyLists[numberOfNode - 1].Add(incidentNode);
        }

        /// <summary>
        /// Строит граф по списку вершин и списку рёбер, соединяющих инцидентные вершины.
        /// </summary>
        /// <param name="listOfEdges">Список рёбер, соединяющих инцидентные вершины</param>
        /// <param name="listOfNodes">Список вершин графа</param>
        public void MakeGraph(List<(int,int)> listOfEdges,List<Node> listOfNodes)
        {
            var list = listOfEdges.GroupBy(tuple => tuple.Item1).ToList();
            if (listOfNodes.Count == 0)
                return;
            listOfEdges.ForEach(tuple => AddEdge(tuple.Item1, listOfNodes[tuple.Item2 - 1]));
        }
        
        public List<Node> this[int index]
        {
            get
            {
                if (index < 0 || index >= adjacencyLists.Count) throw new ArgumentOutOfRangeException();
                return adjacencyLists[index];
            }
        }
    }
}