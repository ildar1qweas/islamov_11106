using System.Collections.Generic;
using System.Linq;
using FinderOfStrongComponents;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace StrongConnectivityTests
{
    [TestClass]
    public class ComponentsTest
    {
        private List<Graph.Node> firstListOfNodes = new()
        {
            new (1),
            new (2),
            new (3),
            new (4),
            new (5),
            new (6),
            new (7)
        };
        
        private List<Graph.Node> secondListOfNodes = new()
        {
            new (1),
            new (2),
            new (3),
            new (4),
        };
        
        private List<(int, int)> threeComponents = new()
        {
            (1, 2), (2, 3), (3, 4), (4, 1),
            (5, 6), (6, 5),
            (1, 7), (7, 6)
        };
        
        private List<(int, int)> manyMultipleEdgesAndLoops = new()
        {
            (1, 2), (1, 2), (1, 2), (2, 3), (3, 4), (4, 1),
            (5, 6), (6, 5), (2, 3), (2, 3), (3, 4), (4, 4), (5, 5),
            (1, 7), (7, 6), (3, 4), (4, 1), (3, 3), (2, 2)
        };

        private List<(int, int)> eachNodeIsComponent = new()
        {
            (1,2), (2,3), (3,4),
            (4,5), (5,6), (6,7)
        };

        private List<(int, int)> oneComponent = new()
        {
            (1, 2), (2, 3), (3, 4), (4, 1)
        };

        [TestMethod]
        public void CorrectGraphMaking()
        {
            var listOfEdges = manyMultipleEdgesAndLoops.GroupBy(tuple => tuple.Item1).ToList();
            var countOfEdges = 0;
            var graph = new Graph(7);
            graph.MakeGraph(manyMultipleEdgesAndLoops,firstListOfNodes);
            for (int i = 0; i < 7; i++)
            {
                foreach (var unused in graph[i])
                    countOfEdges++;
            }
            Assert.AreEqual(manyMultipleEdgesAndLoops.Count, countOfEdges);
            for (int i = 0; i < listOfEdges.Count; i++)
                Assert.AreEqual(listOfEdges[i].Count(),graph[i].Count);
            
        }
        
        [TestMethod]
        public void OneStrongComponentTest()
        {
            var graph = new Graph(secondListOfNodes.Count);
            graph.MakeGraph(oneComponent,secondListOfNodes);
            var components = Searcher.SearchStrongComponents(graph, secondListOfNodes);
            CollectionAssert.AreEqual(secondListOfNodes,components
                .First()
                .OrderBy(node => node.NumberOfNode)
                .ToList());
            CollectionAssert.AllItemsAreUnique(components);
        }
        
        [TestMethod]
        public void EmptyListOfComponentsTest()
        {
            var listOfNodes = new List<Graph.Node>();
            var graph = new Graph(5);
            graph.MakeGraph(new List<(int, int)>(),listOfNodes);
            Assert.AreEqual(0, Searcher.SearchStrongComponents(graph,listOfNodes).Count);
        }

        [TestMethod]
        public void AllNodesAreComponentsTest()
        {
            var graph = new Graph(firstListOfNodes.Count);
            graph.MakeGraph(eachNodeIsComponent,firstListOfNodes);
            var components = Searcher.SearchStrongComponents(graph, firstListOfNodes);
            Assert.AreEqual(firstListOfNodes.Count,components.Count);
            CollectionAssert.AllItemsAreUnique(components);
        }

        [TestMethod]
        public void ConnectedComponents()
        {
            var graph = new Graph(firstListOfNodes.Count);
            graph.MakeGraph(threeComponents, firstListOfNodes);
            var listOfActualComponents = Searcher.SearchStrongComponents(graph, firstListOfNodes);
            var listOfExpectedComponents = new List<List<Graph.Node>>
            {
                new() { firstListOfNodes[0],firstListOfNodes[1],firstListOfNodes[2], firstListOfNodes[3]},
                new() { firstListOfNodes[4], firstListOfNodes[5]},
                new() { firstListOfNodes[6]}
            };
            List<bool> listOfEqualities = new List<bool>();
            foreach (var actualList in listOfActualComponents)
                foreach (var expectedList in listOfExpectedComponents)
                    if(actualList
                       .OrderBy(node => node.NumberOfNode)
                       .SequenceEqual(expectedList
                           .OrderBy(node => node.NumberOfNode)))
                        listOfEqualities.Add(true);
            Assert.AreEqual(listOfExpectedComponents.Count, listOfActualComponents.Count);
            Assert.AreEqual(listOfExpectedComponents.Count,listOfEqualities.Count);
        }

        [TestMethod]
        public void NotContainsSimilarComponents()
        {
            var graph = new Graph(firstListOfNodes.Count);
            graph.MakeGraph(threeComponents, firstListOfNodes);
            var listOfComponents = Searcher.SearchStrongComponents(graph, firstListOfNodes);
            bool notContainsSimilarComponents = true;
            for (int i = 0; i < listOfComponents.Count; i++)
            {
                var countOfSimilarComponents = 0;
                for (int j = i+1; j < listOfComponents.Count; j++)
                {
                    if (listOfComponents[i].SequenceEqual(listOfComponents[j]))
                        countOfSimilarComponents++;
                }

                if (countOfSimilarComponents >= 2)
                {
                    notContainsSimilarComponents = false;
                    break;
                }
            }
            
            CollectionAssert.AllItemsAreUnique(listOfComponents);
            Assert.AreEqual(true, notContainsSimilarComponents);
        }
        
        [TestMethod]
        public void NotContainsSimilarNodesInOneComponent()
        {
            var graph = new Graph(firstListOfNodes.Count);
            graph.MakeGraph(threeComponents, firstListOfNodes);
            var listOfComponents = Searcher.SearchStrongComponents(graph, firstListOfNodes);
            foreach (var list in listOfComponents)
                CollectionAssert.AllItemsAreUnique(list);
        }
    }
}